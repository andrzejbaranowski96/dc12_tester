﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DC12G_Tester.Security
{
    public static class Cipher
    {
        private static string GenerateEncryptionKey()
        {
            string EncryptionKey = string.Empty;
            Random Robj = new Random();
            for(int i = 0; i < 7; i++)
            {
                int val = 0;
                switch (Robj.Next(1, 4))
                {
                    case 1:
                        val = Robj.Next(48, 58);
                        break;
                    case 2:
                        val = Robj.Next(65, 91);
                        break;
                    case 3:
                        val = Robj.Next(97, 123);
                        break;
                }
                EncryptionKey += (char)val;
            }
            return EncryptionKey;
        }

        public static string Encrypt(string clearText)
        {
            string key = GenerateEncryptionKey();
            return Encrypt(clearText, key);
        }

        public static string Decrypt(string cipherText)
        {
            string key = "";
            try
            {
                key = cipherText.Substring(cipherText.Length - 7);
            }
            catch (ArgumentOutOfRangeException)
            {
                return "";
            }
            return Decrypt(cipherText, key);
        }

        private static string Encrypt(string clearText, string EncryptionKey)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText + EncryptionKey;
        }

        private static string Decrypt(string cipherText, string EncryptionKey)
        {
            string key = "";
            try
            {
                key = cipherText.Substring(cipherText.Length - 7);
            }
            catch (ArgumentOutOfRangeException)
            {
                return "";
            }
            cipherText = cipherText.Substring(0, cipherText.Length - 7);
            byte[] cipherBytes = new byte[] { };
            try
            {
                cipherBytes = Convert.FromBase64String(cipherText);
            } catch (FormatException)
            {
                return "";
            }

            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        try
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        } catch (System.Security.Cryptography.CryptographicException)
                        {
                            return "";
                        }
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        #region DoNotRefactor
        private static string Encrypt_UnitTestOnly( string rawData, //Only for unit tests purposes, do not refactor
                                                    string key)
        {
            return Encrypt(rawData, key);
        }

        private static string Decrypt_UnitTestOnly( string encryptedData,  //Only for unit tests purposes, do not refactor
                                                    string key)
        {
            return Decrypt(encryptedData, key);
        }

        private static string GenerateEncryptionKey_UnitTestOnly() //Only for unit tests purposes, do not refactor
        {
            return GenerateEncryptionKey();
        }

        #endregion
    }
}
