﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DC12G_Tester.Miscellaneous
{
    public class Ref<T>     // instance of this class is used to workaround reference passing limitation
                            // because in c# we cannot pass object to task using explicit reference :<
    {
        public Ref() { }
        public Ref(T value) { Value = value; }
        public T Value { get; set; }    // generic value, object that we need to pass
        public override string ToString()
        {
            T value = Value;
            return value == null ? "" : value.ToString();
        }
        public static implicit operator T(Ref<T> r) { return r.Value; } //implicit casting to generic T from reference object r
                                                                        //example usage: int a = new Ref<int>(1);
        public static implicit operator Ref<T>(T value) { return new Ref<T>(value); }   //implicit casting to Ref<T> from generic value T
                                                                                        //example usage: Ref<int> x = 1;
    }
}
