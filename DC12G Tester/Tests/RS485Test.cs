﻿using DC12G_Tester.Files;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static DC12G_Tester.Resources.ResourcesDeployer;

namespace DC12G_Tester.Tests
{
    public sealed class RS485Test : Test
    {
        private SshClient client2;
        private int subPort;

        public RS485Test(int subPort, ref SshClient client)
        {
            this.client2 = client;
            this.subPort = subPort;
            device_path = (subPort == 1) ? "/dev/ttymxc3"     //RS485.1 device path
                                       : "/dev/ttymxc2";    //RS485.2 device path
            baud = 19200;
            consoleOutputStringValues = new Dictionary<string, string>()
            {
                {"testStart", String.Format(GetStringResourceSafely("TestsRunner_Testing_RS48"), subPort)},
                {"speed_DataKeyValue",  (subPort==1)?SETTINGS_KEY_COLLECTION.SPEED_RS485_1 : SETTINGS_KEY_COLLECTION.SPEED_RS485_2},
                {"port_DataKeyValue", (subPort==1)?SETTINGS_KEY_COLLECTION.PORT_RS485_1 : SETTINGS_KEY_COLLECTION.PORT_RS485_2 },
                {"testSucceed", String.Format(GetStringResourceSafely("TestsRunner_RS485_Works"), subPort)},
                {"testFailed", GetStringResourceSafely("TestsRunner_WrongDataReceived") }
            };
            type = TEST_TYPE.SERIAL;
            type_exact = (subPort == 1) ? TEST_TYPE_EXACT.RS_PORT_1 : TEST_TYPE_EXACT.RS_PORT_2;
        }


        public override SshCommand SetRemoteBaud(ref SshClient client)
        {
            if (!client.IsConnected)
                client.Connect();
            try
            {
                return client.RunCommand($"stty {baud} < {device_path}");
            }
            catch (Renci.SshNet.Common.SshConnectionException)
            {
                return null;
            }
        }

        public override SshCommand SendPacket(ref SshClient client)
        {
            return client.RunCommand($"echo -ne '{char_test}' > {device_path}");
        }

        public override bool SetDiode(TEST_RESULT result, ref SshClient client)
        {
            throw new NotImplementedException();
        }
    }
}
