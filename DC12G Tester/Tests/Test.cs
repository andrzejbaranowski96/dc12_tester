﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DC12G_Tester.Tests
{
    public abstract class Test
    {
        public int baud { get; set; }
        public char char_test = '?';
        public string device_path { get; set; }
        public TEST_RESULT result;
        public TEST_TYPE type;
        public TEST_TYPE_EXACT type_exact;
        public Dictionary<string, string> consoleOutputStringValues;
        public abstract SshCommand SetRemoteBaud(ref SshClient client);
        public abstract SshCommand SendPacket(ref SshClient client);
        public abstract bool SetDiode(TEST_RESULT result, ref SshClient client);
    }

    public enum TEST_TYPE
    {
        SERIAL, ETH
    }

    public enum TEST_TYPE_EXACT
    {
        OPTICAL, RS_PORT_1, RS_PORT_2, ETH_PORT_1, ETH_PORT_2 // DO NOT REFACTOR, MUST MATCH WITH CONTROLS IN PANEL
    }

    public enum TEST_RESULT
    {
        SUCCESS, FAILED, TIMEOUT, ERROR_CONNECTION_BROKEN, SKIP, ONGOING, ERROR_TIMEOUT
    }
}
