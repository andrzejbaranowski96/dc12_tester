﻿using DC12G_Tester.Files;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static DC12G_Tester.Resources.ResourcesDeployer;

namespace DC12G_Tester.Tests
{
    public sealed class EthernetPortTest : Test
    {
        private SshClient client2;
        public int subPort;

        public EthernetPortTest(int subPort, ref SshClient client)
        {
            this.client2 = client;
            this.subPort = subPort;
            var data = Settings.GetDataFromFile();
            device_path = (subPort == 1) ? data[SETTINGS_KEY_COLLECTION.ETH_1] : data[SETTINGS_KEY_COLLECTION.ETH_2];
            consoleOutputStringValues = new Dictionary<string, string>()
            {
                {"testStart", String.Format(GetStringResourceSafely("TestsRunner_Testing_Eth"), subPort)},
                {"testSucceed", String.Format(GetStringResourceSafely("TestsRunner_Ethernet_Works"), subPort)},
                {"testFailed", String.Format(GetStringResourceSafely("TestsRunner_Ethernet_PingNotReceived"), device_path) }
            };
            type = TEST_TYPE.ETH;
            type_exact = (subPort == 1) ? TEST_TYPE_EXACT.ETH_PORT_1 : TEST_TYPE_EXACT.ETH_PORT_2;
        }

        public override SshCommand SetRemoteBaud(ref SshClient client)
        {
            throw new NotSupportedException();
        }

        public override SshCommand SendPacket(ref SshClient client)
        {
            throw new NotImplementedException();
        }

        public override bool SetDiode(TEST_RESULT result, ref SshClient client)
        {
            throw new NotImplementedException();
        }
    }
}
