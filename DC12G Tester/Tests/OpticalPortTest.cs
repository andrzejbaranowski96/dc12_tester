﻿using DC12G_Tester.Files;
using Renci.SshNet;
using System.Collections.Generic;
using static DC12G_Tester.Resources.ResourcesDeployer;

namespace DC12G_Tester.Tests
{
    public sealed class OpticalPortTest : Test
    {
        private SshClient client2;
        public OpticalPortTest(ref SshClient client)
        {
            baud = 19200;
            char_test = '*';
            device_path = "/dev/ttymxc5";
            consoleOutputStringValues = new Dictionary<string, string>()
            {
                {"testStart", GetStringResourceSafely("TestsRunner_Testing_Optical")},
                {"speed_DataKeyValue",  SETTINGS_KEY_COLLECTION.SPEED_OPTICAL},
                {"port_DataKeyValue", SETTINGS_KEY_COLLECTION.PORT_OPTICAL },
                {"testSucceed", GetStringResourceSafely("TestsRunner_OpticalPortWorks")},
                {"testFailed", GetStringResourceSafely("TestsRunner_WrongDataReceived") }
            };
            type = TEST_TYPE.SERIAL;
            type_exact = TEST_TYPE_EXACT.OPTICAL;
            this.client2 = client;
        }
        public override SshCommand SetRemoteBaud(ref SshClient client)
        {
            if (!client.IsConnected)
                client.Connect();
            try
            {
                return client.RunCommand($"stty {baud} < {device_path}");
            } catch (Renci.SshNet.Common.SshConnectionException)
            {
                return null;
            }
        }

        public override SshCommand SendPacket(ref SshClient client)
        {
            return client.RunCommand($"echo -ne '{char_test}' > {device_path}");
        }
        public override bool SetDiode(TEST_RESULT result, ref SshClient client)
        {
            switch (result)
            {
                case TEST_RESULT.SUCCESS:
                    
                    
                    break;
                case TEST_RESULT.FAILED:

                    break;
            }
            return false;
        }
    }
}
