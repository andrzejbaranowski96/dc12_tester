﻿using DC12G_Tester.Files;
using DC12G_Tester.Tests;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Windows;
using System.Net.NetworkInformation;
using static DC12G_Tester.Resources.ResourcesDeployer;
using System.Diagnostics;
using DC12G_Tester.Miscellaneous;

namespace DC12G_Tester.Tasks
{
    public class ReceivedValuesEventArgs : EventArgs
    {
        public string message { get; }
        public ReceivedValuesEventArgs(string message)
        {
            this.message = message;
        }
    }
    public class ReceivedStateEventArgs : EventArgs
    {
        public Test T { get; }
        public ReceivedStateEventArgs(Test T)
        {
            this.T = T;
        }
    }
    class TestsRunner : Runner
    {
        public delegate void eventDelegate1(object sender, ReceivedValuesEventArgs e);
        public delegate void eventDelegate2(object sender, ReceivedStateEventArgs e);
        public event eventDelegate1 OnResultReceived;
        public event eventDelegate2 OnStateReceived;

        //public BackgroundWorker worker;
        private SshClient client;
        private Dictionary<string, string> data;

        List<Test> tests;

        private void SendText(string text)
        {
            SendTextNoNewLine(text + Environment.NewLine);
        }

        private void SendTextNoNewLine(string text)
        {
            OnResultReceived?.Invoke(this, new ReceivedValuesEventArgs(text));
        }

        private void IndicateResultInUI(Test t)
        {
            OnStateReceived?.Invoke(this, new ReceivedStateEventArgs(t));
        }

        public TestsRunner( ref SshClient client)
        {
            this.client = client;
            this.Init();
        }

        internal override void Init()
        {
            base.Init();
            worker.WorkerReportsProgress = true;
        }

        private void InitTests()
        {
            if (data == null) throw new NullReferenceException("data is null, fill data first");
            tests = new List<Test>()
            {
                new OpticalPortTest(ref client),
                new RS485Test(1, ref client),
                new RS485Test(2, ref client),
                new EthernetPortTest(1, ref client),
                new EthernetPortTest(2, ref client)
            };
        }

        protected override void TaskToRun()
        {
            data = Settings.GetDataFromFile();
            TEST_RESULT result;

            InitTests();
            foreach (Test t in tests)
            {
                t.result = TEST_RESULT.ONGOING;
                result = TEST_RESULT.ERROR_CONNECTION_BROKEN;
                t.consoleOutputStringValues.TryGetValue("testStart", out string startValue);
                SendText(startValue);
                if (t.type == TEST_TYPE.SERIAL) {
                    t.consoleOutputStringValues.TryGetValue("speed_DataKeyValue", out string baudValue);
                    t.baud = Int32.Parse(data[baudValue]);
                    SendText($"{Application.Current.Resources["TestsRunner_BaudSpeedSet"]} " + $"{t.baud}.");
                }
                IndicateResultInUI(t);
                t.result = result;
                SendTextNoNewLine(GetStringResourceSafely("TestsRunner_BeginTests"));
                for (int i = 0; i < 5; i++)
                {
                    SendTextNoNewLine(".");
                    result = ExecuteTest(t);
                    t.result = result;
                    if (result != TEST_RESULT.SUCCESS) break;
                }

                IndicateResultInUI(t);

                SendText("");
                SendText(GetMessage(result, t));
                SendText("");
                if (worker.CancellationPending 
                    || result==TEST_RESULT.ERROR_CONNECTION_BROKEN) return;
            }

            SendText($"{Application.Current.Resources["TestsRunner_TestsFinished"]}");
        }

        private TEST_RESULT ExecuteTest(Test t)
        {
            if (t.type == TEST_TYPE.SERIAL)
            {
                t.consoleOutputStringValues.TryGetValue("port_DataKeyValue", out string portValue);
                string portname = data[portValue];
                if (portname.Equals("NULL"))
                {
                    
                    return TEST_RESULT.SKIP;
                }

                SerialPort port = GetPort(portname, t);
                if(port==null || !port.IsOpen)
                {
                    SendText(String.Format(Application.Current.Resources["TestsRunner_Abort"].ToString(), data[SETTINGS_KEY_COLLECTION.PORT_OPTICAL]));
                    return TEST_RESULT.ERROR_CONNECTION_BROKEN;
                }
                TEST_RESULT result = PerformTestCOM(t, port);
                port.Close();
                port.Dispose();
                return result;
            }
            else if (t.type == TEST_TYPE.ETH)
            {
                Ping ping = new Ping();
                Task<PingReply> task = ping.SendPingAsync(t.device_path);
                while (!task.IsCompleted && !worker.CancellationPending) { }
                if (worker.CancellationPending)
                    return TEST_RESULT.SKIP;
                if (task.Result.Status == IPStatus.Success)
                {
                    return TEST_RESULT.SUCCESS;
                }
                
                return TEST_RESULT.FAILED;
            }
            return TEST_RESULT.ERROR_CONNECTION_BROKEN;
        }

        private SerialPort GetPort(string PortName, Test test)
        {
            SerialPort port;
            try
            {
                port = new SerialPort(PortName);
                port.BaudRate = test.baud;
                port.Open();
            }
            catch
            {
                port = null;
            }
            return port;
        }
        private TEST_RESULT PerformTestCOM(Test test, SerialPort port)
        {
            if (test.baud == 0)
                test.baud = 9600;
            try
            {
                test.SetRemoteBaud(ref client);
                test.SendPacket(ref client);
                int result = ReadDataIfAvailable(new Ref<SerialPort>(port), test).Result;
                if (result == -1) return TEST_RESULT.TIMEOUT;
                if (result.Equals(test.char_test)) return TEST_RESULT.SUCCESS;
                return TEST_RESULT.FAILED;
            }
            catch (Renci.SshNet.Common.SshConnectionException)
            {
                return TEST_RESULT.ERROR_CONNECTION_BROKEN;
            }
        }

        bool timeout = false;
        private Task<int> ReadDataIfAvailable(Ref<SerialPort> port, Test t)
        {
            Task<int> task = new Task<int>(() =>
            {
                int retries = 0;
                timeout = false;
                Timer timer = new Timer(2000);
                timer.Elapsed += Timer_Elapsed;
                timer.Start();
                while (port.Value.BytesToRead == 0 && !timeout && !worker.CancellationPending) {
                    System.Threading.Thread.Sleep(100);
                    t.SendPacket(ref client);
                    retries++;
                }
                timer.Stop();
                if (timeout || worker.CancellationPending) return -1;
                if(retries > 0)
                    SendText(String.Format(Application.Current.Resources
                        ["TestsRunner_Warning_ConnectionMayBeNotReliable"].ToString(),
                    retries));
                return port.Value.ReadByte();
            });
            task.Start();
            return task;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timeout = true;
        }

        private string GetMessage(TEST_RESULT result, Test t)
        {
            Ref<int> a = 1;
            int x = new Ref<int>(1);
            if (worker.CancellationPending) return Environment.NewLine + GetStringResourceSafely("TestsRunner_TestsCancelled");
            switch (result)
            {
                case TEST_RESULT.SUCCESS:
                    t.consoleOutputStringValues.TryGetValue("testSucceed", out string successValue);
                    return successValue;
                case TEST_RESULT.FAILED:
                    t.consoleOutputStringValues.TryGetValue("testFailed", out string failedValue);
                    return failedValue;
                case TEST_RESULT.TIMEOUT:

                    return GetStringResourceSafely("TestsRunner_Timeout");
                case TEST_RESULT.SKIP:
                    return GetStringResourceSafely("TestsRunner_PortIsNull");
                case TEST_RESULT.ERROR_CONNECTION_BROKEN:
                    return GetStringResourceSafely("TestsRunner_ConnectionBroken");
                case TEST_RESULT.ERROR_TIMEOUT:
                    return GetStringResourceSafely("TestsRunner_ConnectionTimeout");
                default:
                    return string.Format(GetStringResourceSafely("TestsRunner_ResponseNotDefined"), Enum.GetName(typeof(TEST_RESULT), result));
            }
            
        }
    }
}
