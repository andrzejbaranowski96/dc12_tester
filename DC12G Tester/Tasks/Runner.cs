﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DC12G_Tester.Tasks
{
    abstract class Runner
    {
        public BackgroundWorker worker;

        internal virtual void Init()
        {
            worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += Worker_DoWork;
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            TaskToRun();
        }

        public virtual void Run()
        {
            try
            {
                worker.RunWorkerAsync();
            }
            catch (InvalidOperationException) //worker is still busy, force close
            {
                worker.Dispose();
                Init();
                worker.RunWorkerAsync();
            }
        }

        public virtual void Cancel()
        {
            worker.CancelAsync();
        }

        protected abstract void TaskToRun();
    }
}
