﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DC12G_Tester.Files;
using static DC12G_Tester.Resources.ResourcesDeployer;
using DC12G_Tester.Miscellaneous;
using System.Windows.Threading;

namespace DC12G_Tester.Tasks
{
    #region eventargs

    public class MessagePushedEventArgs : EventArgs
    {
        public readonly string message;
        public MessagePushedEventArgs(string message)
        {
            this.message = message;
        }
    }

    public class RequestedWindowStateChangeEventArgs : EventArgs
    {
        public readonly Windows.DBOPERATIONSWINDOW_STATE state;
        public RequestedWindowStateChangeEventArgs(Windows.DBOPERATIONSWINDOW_STATE state)
        {
            this.state = state;
        }
    }

    public class DataPushedEventArgs : EventArgs
    {
        public readonly Dictionary<string, string> data;

        public DataPushedEventArgs(Dictionary<string, string> data)
        {
            this.data = data;
        }
    }

    public class RequestedActionEventArgs<T> : EventArgs
    {
        private Task<T> requestedAction;
        public RequestedActionEventArgs(Task<T> action)
        {
            requestedAction = action;
        }
        public Task<T> GetAndRunAction()
        {
            requestedAction.Start();
            return requestedAction;
        }
    }

    #endregion

    class DBConnectionRunner : Runner
    {
        #region events
        public delegate void MessagePushedDelegate(object sender, MessagePushedEventArgs e);
        public event MessagePushedDelegate MessagePushed;

        public delegate void RequestedWindowStateChangeDelegate(object sender, RequestedWindowStateChangeEventArgs e);
        public event RequestedWindowStateChangeDelegate RequestedWindowStateChange;

        public delegate void DataPushedDelegate(object sender, DataPushedEventArgs e);
        public event DataPushedDelegate DataPushed;

        public delegate void RequestedActionDelegate(object sender, DataPushedEventArgs e);
        public event RequestedActionDelegate RequestedAction;
        #endregion

        private SqlConnection conn;
        private string productID;
        private List<string> storedConnectionValues;
        /*  0 - IP address
         *  1 - starting folder (need to change name from database name)
         *  2 - username
         *  3 - password (encrypt/decrypt that)
        */

        private void PushChangeRequest(Windows.DBOPERATIONSWINDOW_STATE state)
        {
            RequestedWindowStateChange.Invoke(this, new RequestedWindowStateChangeEventArgs(state));
        }

        private void PushData(Dictionary<string, string> data)
        {
            DataPushed.Invoke(this, new DataPushedEventArgs(data));
        }
        
        private void PushMessage(string message)
        {
            MessagePushed.Invoke(this, new MessagePushedEventArgs(message + Environment.NewLine));
            // ternary operator
            // (<bool_condition>) ? <if_condition_is_true> : <if_confition_is_false>
        }

        private void PushMessageNoNewLine(string message)
        {
            MessagePushed.Invoke(this, new MessagePushedEventArgs(message));
        }

        public DBConnectionRunner(string productID)
        {
            this.productID = productID;
            Init();
        }

        protected override void TaskToRun()
        {
            string query = "SELECT * FROM [test1_test].[dbo].[dtproperties] WHERE [id] = @ID";
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@ID", productID);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    Dictionary<string, string> dataFromDB = new Dictionary<string, string>();
                    for(int i = 0; i < reader.FieldCount; i++)
                    {
                        string value = "";
                        var a = reader.GetSqlValue(i);
                        if(a != null)
                        {
                            value = a.ToString();
                        }
                        dataFromDB.Add(reader.GetName(i), value);
                    }
                    PushChangeRequest(Windows.DBOPERATIONSWINDOW_STATE.VERIFICATION);
                    PushData(dataFromDB);
                }
            }
        }

        internal override void Init()
        {
            base.Init();
            var dict = Files.Settings.GetDataFromFile();
            storedConnectionValues = new List<string>()
            {
                dict[SETTINGS_KEY_COLLECTION.DB_IP_ADDRESS],
                dict[SETTINGS_KEY_COLLECTION.DB_NAME],
                dict[SETTINGS_KEY_COLLECTION.DB_USERNAME],
                Security.Cipher.Decrypt(
                    dict[SETTINGS_KEY_COLLECTION.DB_PASSWORD].ToString())
            };
        }



        private SqlConnection Connect()
        {
            string connString = "Data Source=" + storedConnectionValues[0] + ";Initial Catalog=" + storedConnectionValues[1]
                + ";Persist Security Info=True;User ID=" + storedConnectionValues[2]
                + ";Password=" + storedConnectionValues[3] + ";"; 
            conn = new SqlConnection(connString);
            try
            {
                conn.Open();
            } catch (SqlException)
            {
                conn = null;
            }
            return conn;
        }

        private Task<SqlConnection> RunConnAsync(SqlConnection conn)
        {
            Task<SqlConnection> task = new Task<SqlConnection>(() =>
            {
                if (conn == null)
                {
                    conn = Connect();
                }
                else if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }
                return conn;
            });
            task.Start();
            return task;
        }

        public void Run(ref SqlConnection conn)
        {
            Ref<SqlConnection> c = new Ref<SqlConnection>(conn);
            Thread t = new Thread(()=>
            {
                PushMessage(GetStringResourceSafely("DBConnectionRunner_Connecting"));
                Task<SqlConnection> task = RunConnAsync(c.Value);
                bool stoppingRequest = false;
                while (!task.IsCompleted && !stoppingRequest) {
                    Thread.Sleep(100);
                    if(!Dispatcher.CurrentDispatcher.CheckAccess())
                        Dispatcher.CurrentDispatcher.Invoke(() => 
                        { stoppingRequest = worker.CancellationPending; });
                }
                if (task.Result != null)
                {
                    c.Value = task.Result;
                    this.conn = c.Value;
                    if (this.conn.State == System.Data.ConnectionState.Open)
                    {
                        PushMessage(GetStringResourceSafely("DBConnectionRunner_Connected"));
                        base.Run();
                    }
                }
                if (task.Result==null || task.Result.State != System.Data.ConnectionState.Open) {
                    PushMessage(string.Format(
                            GetStringResourceSafely("DBConnectionRunner_NoConnection"),
                            storedConnectionValues[0], storedConnectionValues[1]));
                }
            });
            t.IsBackground = true;
            t.Start();
        }
    }
}

