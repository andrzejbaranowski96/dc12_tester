﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DC12G_Tester.Windows
{
    /// <summary>
    /// Interaction logic for ImportWindow.xaml
    /// </summary>
    public partial class ImportWindow : Window
    {
        private SqlConnection sqlconn;
        public ImportWindow(ref SqlConnection sqlconn)
        {
            InitializeComponent();
            this.sqlconn = sqlconn;
            textBox_codeInput.Focus();
        }

        private void Button_OK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            DBOperationsWindow dbOpsWindow = new DBOperationsWindow(textBox_codeInput.Text, ref sqlconn);
            dbOpsWindow.Show();
            this.Close();
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
