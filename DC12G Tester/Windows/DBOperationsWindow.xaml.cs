﻿using DC12G_Tester.Tasks;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DC12G_Tester.Windows
{
    public partial class DBOperationsWindow : Window
    {
        private DBConnectionRunner runner;
        private SqlConnection sqlconn;
        public DBOperationsWindow(string productID, ref SqlConnection sqlconn)
        {
            InitializeComponent();
            this.sqlconn = sqlconn;
            runner = new DBConnectionRunner(productID);
            runner.MessagePushed += Runner_MessagePush;
            runner.RequestedWindowStateChange += Runner_RequestWindowStateChange;
            runner.DataPushed += Runner_DataPush;
        }

        public new void Show()
        {
            base.Show();
            runner.Run(ref sqlconn);
        }

        private UIElement GetVisibleElement()
        {
            foreach (UIElement element in grid.Children)
            {
                if (element.Visibility == Visibility.Visible)
                {
                    return element;
                }
            }
            return null;
        }

        private void Runner_DataPush(object sender, DataPushedEventArgs e)
        {
            if (!CheckAccess())
            {
                Dispatcher.Invoke(() => {
                    UIElement visibleElement = GetVisibleElement();
                    if (visibleElement != null)
                    {
                        if (visibleElement.GetType() == verificationControl.GetType())
                        {
                            verificationControl.SetDataToControl(e.data);
                        }
                    }
                });
            }
        }

        private void Runner_RequestWindowStateChange(object sender, RequestedWindowStateChangeEventArgs e)
        {
            if (!CheckAccess())
            { 
                Dispatcher.Invoke(() => SwitchVisibleControl(e.state));
            }
        }

        private void Runner_MessagePush(object sender, MessagePushedEventArgs e)
        {
            if (!CheckAccess())
            {
                Dispatcher.Invoke(() => processingControl.SetStatus(e.message));
            }
        }

        private void SwitchVisibleControl(DBOPERATIONSWINDOW_STATE state)
        {
            foreach (UIElement element in grid.Children)
            {
                element.Visibility = Visibility.Hidden;
            }
            switch (state)
            {
                case DBOPERATIONSWINDOW_STATE.PROCESSING:
                    {
                        processingControl.Visibility = Visibility.Visible;
                        break;
                    }
                case DBOPERATIONSWINDOW_STATE.VERIFICATION:
                    {
                        verificationControl.Visibility = Visibility.Visible;
                        break;
                    }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            runner.Cancel();
        }
    }

    public enum DBOPERATIONSWINDOW_STATE
    {
        PROCESSING, VERIFICATION
    }
}
