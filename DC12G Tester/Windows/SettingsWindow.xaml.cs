﻿using DC12G_Tester.Files;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static DC12G_Tester.Security.Cipher;

namespace DC12G_Tester.Windows
{
    public class DataEventArgs : EventArgs
    {
        public readonly Dictionary<string, string> data;
        public DataEventArgs(Dictionary<string, string> data)
        {
            this.data = data;
        }
    }
    public partial class SettingsWindow : Window
    {
        public delegate void SaveData(object sender, DataEventArgs e);

        private Dictionary<string, string> data;
        public SettingsWindow(Dictionary<string, string> data)
        {
            InitializeComponent();
            this.data = data;
            InitPorts();
            InitSpeeds();
            InitEths();
            InitDB();
        }

        private void InitDB()
        {
            dbControl.textBox_databaseIP.Uid = SETTINGS_KEY_COLLECTION.DB_IP_ADDRESS;
            dbControl.textBox_databaseName.Uid = SETTINGS_KEY_COLLECTION.DB_NAME;
            dbControl.textBox_username.Uid = SETTINGS_KEY_COLLECTION.DB_USERNAME;
            dbControl.textBox_password.Uid = SETTINGS_KEY_COLLECTION.DB_PASSWORD;

            foreach(Control c in dbControl.controls)
            {
                if (c is TextBox)
                {
                    (c as TextBox).Text = data[c.Uid];
                }
                else if (c is PasswordBox)
                {
                    (c as PasswordBox).Password = Decrypt(data[c.Uid]);
                }
                c.LostFocus += DBInternalControl_LostFocus;
            }
        }


        private void InitEths()
        {
            ethsControl.textBox_ethernet1.Uid = SETTINGS_KEY_COLLECTION.ETH_1;
            ethsControl.textBox_ethernet2.Uid = SETTINGS_KEY_COLLECTION.ETH_2;

            foreach (TextBox box in ethsControl.textBoxes)
            {
                box.Text = data[box.Uid];
                box.LostFocus += Box_LostFocus;
            }
        }


        private void InitPorts()
        {
            portsControl.comboBox_Optical.Uid = SETTINGS_KEY_COLLECTION.PORT_OPTICAL;
            portsControl.comboBox_RS485_1.Uid = SETTINGS_KEY_COLLECTION.PORT_RS485_1;
            portsControl.comboBox_RS485_2.Uid = SETTINGS_KEY_COLLECTION.PORT_RS485_2;

            List<string> ports =(System.IO.Ports.SerialPort.GetPortNames()).Concat((new string[] { "NULL" })).ToList();
            foreach (ComboBox box in portsControl.comboBoxes)
            {
                box.ItemsSource = ports;
                box.SelectedIndex = (data[box.Uid].Equals(""))
                    ? ports.Count - 1
                    : ports.IndexOf(data[box.Uid]);
                box.SelectionChanged += ComboBox_SelectionChanged;
            }
        }


        private void InitSpeeds()
        {
            speedsControl.comboBox_Optical.Uid = SETTINGS_KEY_COLLECTION.SPEED_OPTICAL;
            speedsControl.comboBox_RS485_1.Uid = SETTINGS_KEY_COLLECTION.SPEED_RS485_1;
            speedsControl.comboBox_RS485_2.Uid = SETTINGS_KEY_COLLECTION.SPEED_RS485_2;
            List<string> speeds = new List<string>()
            {
                "9600", "19200"
            };
            foreach (ComboBox box in speedsControl.comboBoxes)
            {
                box.ItemsSource = speeds;
                box.SelectedIndex = (data[box.Uid].Equals(""))
                    ? 0
                    : speeds.IndexOf(data[box.Uid]);
                box.SelectionChanged += ComboBox_SelectionChanged;
            }
        }

        #region EVENTS_HANDLERS
        private bool ValidateIP(string ip)
        {
            if (IPAddress.TryParse(ip, out IPAddress _ip)) return true;
            return false;
        }

        private void Box_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = sender as TextBox;
            if (ValidateIP(box.Text))
            {
                
                data[box.Uid] = box.Text;
                ethsControl.label_warning.Visibility = Visibility.Hidden;
                box.Background = new TextBox().Background;
            }
            else
            {
                ethsControl.label_warning.Visibility = Visibility.Visible;
                box.Background = Brushes.LightPink;
            }
        }
        private void DBInternalControl_LostFocus(object sender, RoutedEventArgs e)
        {
            Control c = sender as Control;
            try
            {
                var txtbox = c as TextBox;
                data[txtbox.Uid] = txtbox.Text;
            }
            catch
            {
                var pwdbox = c as PasswordBox;
                data[pwdbox.Uid] = Encrypt(pwdbox.Password);
            }
        }
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            data[box.Uid] = box.SelectedItem.ToString();
        }

        private void Button_OK_Click(object sender, RoutedEventArgs e)
        {
            FileOperations.WriteToFile(data, PATHS.SETTINGS);
            DialogResult = true;
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        #endregion
    }
}
