﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using Renci.SshNet;
using DC12G_Tester.Tests;
using DC12G_Tester.Tasks;
using DC12G_Tester.Files;
using System.Windows.Controls;
using static DC12G_Tester.Resources.ResourcesDeployer;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Data.SqlClient;

namespace DC12G_Tester.Windows
{
    public partial class TestWindow : Window
    {
        public readonly Window parent;
        private SshClient connection;
        private SqlConnection sqlconn;

        private TestsRunner testsWorker;

        SettingsWindow settingsWindow;


        public TestWindow(Window parent, ref SshClient conn)
        {
            InitializeComponent();
            this.parent = parent;
            connection = conn;
            testsWorker = new TestsRunner(ref connection);
            testsWorker.OnResultReceived += TestsWorker_OnResultReceived;
            testsWorker.OnStateReceived += TestsWorker_OnStateReceived;
        }

        private void TestsWorker_OnResultReceived(object sender, ReceivedValuesEventArgs e)
        {
            if (!CheckAccess())
            {
                Dispatcher.Invoke(() =>
                {
                    textBox_outputConsole.Text += e.message;
                    textBox_outputConsole.ScrollToEnd();
                });
            }
        }
        private void TestsWorker_OnStateReceived(object sender, ReceivedStateEventArgs e)
        {
            if (!CheckAccess())
            {
                Dispatcher.Invoke(() =>
                {
                    string port = e.T.type_exact.ToString();
                    var elllipse = (Ellipse)this.FindName(port);
                    if(e.T.result == TEST_RESULT.SUCCESS)
                    {
                        elllipse.Fill = new SolidColorBrush(Colors.Green);
                    }
                    else if (e.T.result == TEST_RESULT.ONGOING)
                    {
                        elllipse.Fill = new SolidColorBrush(Colors.Yellow);
                    }
                    else
                    {
                        elllipse.Fill = new SolidColorBrush(Colors.Red);
                    }
                });
            }
        }

        private void TestWindow1_Closed(object sender, EventArgs e)
        {
            parent.Show();
        }

        private void Button_Settings_Click(object sender, RoutedEventArgs e)
        {
            settingsWindow = new SettingsWindow(Settings.GetDataFromFile());
            settingsWindow.ShowDialog();
        }

        private void ClearEllipse()
        {
            foreach(object o in LogicalTreeHelper.GetChildren(this.grid))
            {
                if(o.GetType() == typeof(Ellipse))
                {
                    (o as Ellipse).Fill = new SolidColorBrush(Color.FromRgb(244,244,245));
                }
            }
        }

        private void Button_StartStop_Click(object sender, RoutedEventArgs e)
        {
            if ((button_StartStop.Content as TextBlock).Text.Equals(Application.Current.Resources["TestWindow_Button_Start"]))
            {
                ClearEllipse();
                textBox_outputConsole.Text = "";
                testsWorker.Run();
                testsWorker.worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
                (button_StartStop.Content as TextBlock).Text
                    = GetStringResourceSafely("TestWindow_Button_Stop");
                button_Settings.IsEnabled = false;
                button_InsertData.IsEnabled = false;
            }
            else
            {
                testsWorker.Cancel();
                (button_StartStop.Content as TextBlock).Text
                    = GetStringResourceSafely("TestWindow_Button_Stopping");
            }
        }

        private void Worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            (button_StartStop.Content as TextBlock).Text
                    = GetStringResourceSafely("TestWindow_Button_Start");
            button_Settings.IsEnabled = true;
            button_InsertData.IsEnabled = true;
            testsWorker.worker.Dispose();
        }

        private void Button_InsertData_Click(object sender, RoutedEventArgs e)
        {
            ImportWindow window = new ImportWindow(ref sqlconn);
            window.ShowDialog();
        }
    }
}
