﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Renci.SshNet;
using System.Net.Sockets;
using DC12G_Tester.Windows;
using static DC12G_Tester.Files.LastLogin;
using DC12G_Tester.Files;
using static DC12G_Tester.Security.Cipher;
using System.Collections.ObjectModel;

namespace DC12G_Tester
{
    public sealed partial class MainWindow : Window
    {
        public SshClient connection;
        public static string[] connectionInfo;
        public MainWindow()
        {
            InitializeComponent();
            var list = readFromFile();
            SetToWindow(list);
            comboBox_language.SelectedItem = (ComboBoxItem)comboBox_language.FindName(list[4]);

        }

        private List<String> readFromFile()
        {
            var dict = GetDataFromFile();
            if (dict[LASTLOGIN_KEY_COLLECTION.CHECKBOX] == "True")
            {
                List<string> parameters = new List<string>()    
                            //order is important, append new values to bottom
                {
                    dict[LASTLOGIN_KEY_COLLECTION.IPADDRESS],
                    dict[LASTLOGIN_KEY_COLLECTION.USERNAME],
                    Decrypt(dict[LASTLOGIN_KEY_COLLECTION.PASSWORD]),
                    dict[LASTLOGIN_KEY_COLLECTION.CHECKBOX],
                    dict[LASTLOGIN_KEY_COLLECTION.LANGUAGE]
                };
                return parameters;
            }
            else
            {
                List<string> parameters = new List<string>();
                for (int i = 0; i < LASTLOGIN_KEY_COLLECTION.COLLECTION.Count; i++)
                {
                    parameters.Add("");
                }
                return parameters;
            }

        }

        private void SetToWindow(List<String> parameters)
        {
            textBox_ip.Text = parameters[0];
            textBox_username.Text = parameters[1];
            textBox_password.Password = parameters[2];
            if (parameters[3] == "True")
            {
                checkBox_remember.IsChecked = true;
            }
        }

        private void WriteToFile()
        {
            var dict = GetDataFromFile();
            dict[LASTLOGIN_KEY_COLLECTION.IPADDRESS] = textBox_ip.Text;
            dict[LASTLOGIN_KEY_COLLECTION.USERNAME] = textBox_username.Text;
            dict[LASTLOGIN_KEY_COLLECTION.PASSWORD] = Encrypt(textBox_password.Password);
            dict[LASTLOGIN_KEY_COLLECTION.CHECKBOX] = checkBox_remember.IsChecked.ToString();
            dict[LASTLOGIN_KEY_COLLECTION.LANGUAGE] = (comboBox_language.SelectedItem as ComboBoxItem).Content.ToString();

            FileOperations.WriteToFile(dict, PATHS.LASTLOGIN);
        }
        private async void Button_connect_Click(object sender, RoutedEventArgs e)
        {
            if (IsValid(textBox_ip) &&
                IsValid(textBox_username) &&
                IsValid(textBox_password))
            {
                SshClient client = new SshClient(textBox_ip.Text, textBox_username.Text, textBox_password.Password);
                label_info.Content = Application.Current.Resources["MainWindow_Info_Connecting"];
                button_connect.IsEnabled = false;
                await Task.Run(() =>
                {
                    try
                    {
                        client.Connect();
                    }
                    catch (SocketException socEx)
                    {
                        Console.WriteLine(socEx.Message);
                        Console.Write(socEx.StackTrace);
                        if (!CheckAccess())
                        {
                            Dispatcher.Invoke(() => { label_info.Content = Application.Current.Resources["MainWindow_Info_CannotConnect"]; });
                        }
                    }
                    catch (Renci.SshNet.Common.SshAuthenticationException sshaEx)
                    {
                        Console.WriteLine(sshaEx.Message);
                        Console.Write(sshaEx.StackTrace);
                        if (!CheckAccess())
                        {
                            Dispatcher.Invoke(() => { label_info.Content = Application.Current.Resources["MainWindow_Info_WrongLoginData"]; });
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.Write(ex.StackTrace);
                        if (!CheckAccess())
                        {
                            Dispatcher.Invoke(() => { label_info.Content = Application.Current.Resources["MainWindow_Info_CannotConnect"]; });
                        }
                    }
                });
                if (client.IsConnected)   //TODO: IF connection successful
                {
                    label_info.Content = Application.Current.Resources["MainWindow_Info_Connected"];
                    connection = client;
                    connectionInfo = new string[]
                    {
                        textBox_ip.Text, textBox_username.Text, textBox_password.Password
                    };

                    WriteToFile();

                    TestWindow window = new TestWindow(this, ref connection);
                    window.Show();
                    this.Hide();
                    //textBox_password.Password = "";
                    label_info.Content = Application.Current.Resources["MainWindow_Info_NotConnected"];
                }
            }
            else label_info.Content = Application.Current.Resources["MainWindow_Info_Error"];
            button_connect.IsEnabled = true;
        }


        private bool IsValid(Control control)
        {
            if (control.Name == textBox_ip.Name)
            {
                TextBox pass = control as TextBox;
                if (pass.Text.Equals(""))
                {
                    ChangeBorderStyle(control, ControlState.NOTVALID);
                    return false;
                }
                ChangeBorderStyle(control, ControlState.VALID);
                return true;
            }
            else if (control.Name == textBox_username.Name)
            {
                TextBox username = control as TextBox;
                if (username.Text.Equals(""))
                {
                    ChangeBorderStyle(control, ControlState.NOTVALID);
                    return false;
                }
                ChangeBorderStyle(control, ControlState.VALID);
                return true;
            }
            else if (control.Name == textBox_password.Name)
            {

                return true;
            }
            else
            {
                throw new NotImplementedException("Unknown control");
            }
        }

        private void ChangeBorderStyle(Control control, ControlState state)
        {
            switch (state)
            {
                case ControlState.NOTVALID:
                    control.Background = Brushes.LightPink;
                    break;
                case ControlState.VALID:
                    control.Background = new TextBox().Background;
                    break;
            }
        }

        private void ComboBox_language_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (ResourceDictionary rd in Application.Current.Resources.MergedDictionaries)
            {
                string src = rd.Source.ToString();
                string newsrc = "";
                try
                {
                    newsrc = src.Substring(0, src.IndexOf("/") + 1) +
                    ((sender as ComboBox).SelectedItem as ComboBoxItem).Content +
                    src.Substring(src.LastIndexOf("/"));

                }
                catch (NullReferenceException)
                {
                    newsrc = src.Substring(0, src.IndexOf("/") + 1) +
                    "en" +
                    src.Substring(src.LastIndexOf("/"));
                }
                try
                {
                    rd.Source = new Uri(newsrc, UriKind.Relative);
                }
                catch (System.IO.IOException)
                {   // Resource file not found, loading default, english resource
                    string newsrc_eng = src.Substring(0, src.IndexOf("/") + 1) +
                        "en" +
                        src.Substring(src.LastIndexOf("/"));
                    rd.Source = new Uri(newsrc_eng, UriKind.Relative);
                }
            }

        }
    }

    enum ControlState
    {
        VALID, NOTVALID
    }
}
