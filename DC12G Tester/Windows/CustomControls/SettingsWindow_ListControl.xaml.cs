﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DC12G_Tester.Windows.CustomControls
{
    /// <summary>
    /// Interaction logic for SettingsWindow_ListControl.xaml
    /// </summary>
    public partial class SettingsWindow_ListControl : UserControl
    {
        public readonly ComboBox[] comboBoxes;
        public SettingsWindow_ListControl()
        {
            InitializeComponent();
            comboBoxes = new ComboBox[]
            {
                comboBox_Optical, comboBox_RS485_1, comboBox_RS485_2
            };
        }
    }
}
