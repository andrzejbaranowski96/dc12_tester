﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DC12G_Tester.Windows.CustomControls
{
    /// <summary>
    /// Interaction logic for DBOperationsWindow_VerificationControl_ReceivedDataPresentationPanel.xaml
    /// </summary>
    public partial class DBOperationsWindow_VerificationControl_Panel : UserControl
    {
        public DBOperationsWindow_VerificationControl_Panel()
        {
            InitializeComponent();
        }

        public DBOperationsWindow_VerificationControl_Panel(string header, string value)
        {
            InitializeComponent();
            SetHeader(header);
            SetValue(value);
        }

        public void SetHeader(string header)
        {
            textBlock_Title.Text = header;
        }

        public void SetValue(string value)
        {
            textBlock_Value.Text = value;
        }
    }
}
