﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DC12G_Tester.Windows.CustomControls
{
    /// <summary>
    /// Interaction logic for DBOperationsWindow_VerificationControl.xaml
    /// </summary>
    public partial class DBOperationsWindow_VerificationControl : UserControl
    {
        public DBOperationsWindow_VerificationControl()
        {
            InitializeComponent();
        }

        public void SetDataToControl(Dictionary<string, string> data)
        {
            foreach(var pair in data)
            {
                var panel = new DBOperationsWindow_VerificationControl_Panel
                    (pair.Key, 
                    pair.Value);
                panel.Width = double.NaN;
                panel.Height = double.NaN;
                stackPanel_receivedData.Children.Add(panel);
            }
        }
    }
}
