﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DC12G_Tester.Windows.CustomControls
{
    public partial class SettingsWindow_EthernetIPControl : UserControl
    {
        public readonly TextBox[] textBoxes;
        public SettingsWindow_EthernetIPControl()
        {
            InitializeComponent();
            textBoxes = new TextBox[]
            {
                textBox_ethernet1, textBox_ethernet2
            };
        }
    }
}
