﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DC12G_Tester.Files
{
    internal static class SETTINGS_KEY_COLLECTION
    {
        internal static List<string> COLLECTION = new List<string>()
        {
            PORT_OPTICAL, PORT_RS485_1, PORT_RS485_2,
            SPEED_OPTICAL, SPEED_RS485_1, SPEED_RS485_2,
            ETH_1, ETH_2, DB_IP_ADDRESS,
            DB_NAME, DB_USERNAME, DB_PASSWORD
        };

        internal const string PORT_OPTICAL = "PORT_OPTICAL";
        internal const string PORT_RS485_1 = "PORT_RS485.1";
        internal const string PORT_RS485_2 = "PORT_RS485.2";
        internal const string SPEED_OPTICAL = "SPEED_OPTICAL";
        internal const string SPEED_RS485_1 = "SPEED_RS485.1";
        internal const string SPEED_RS485_2 = "SPEED_RS485.2";
        internal const string ETH_1 = "ETHERNET_1";
        internal const string ETH_2 = "ETHERNET_2";

        internal const string DB_IP_ADDRESS = "DB_ADDR";
        internal const string DB_NAME = "DB_NAME";
        internal const string DB_USERNAME = "DB_USRNAME";
        internal const string DB_PASSWORD = "DB_PSSWRD";
    }

    internal static class LASTLOGIN_KEY_COLLECTION
    {
        internal static List<string> COLLECTION = new List<string>()
        {
            IPADDRESS, USERNAME, PASSWORD, CHECKBOX, LANGUAGE
        };

        internal const string IPADDRESS = "IPADDR";
        internal const string USERNAME = "USRNAME";
        internal const string PASSWORD = "PSSWRD";
        internal const string CHECKBOX = "CHCKBOX";
        internal const string LANGUAGE = "LANG";
    }
}
