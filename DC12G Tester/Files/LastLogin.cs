﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DC12G_Tester.Files.FileOperations;
using static DC12G_Tester.Resources.ResourcesDeployer;

namespace DC12G_Tester.Files
{
    class LastLogin
    {
        private static List<string> _keys;
        public static Dictionary<string, string> GetDataFromFile(string path, List<string> keys)
        {
            _keys = keys;
            StreamReader fileToRead = null;
            Dictionary<string, string> dictionary = GenerateDefaultDictionary();
            //Application.Current.Resources
            //["SettingsErrors_Loading_Default"].ToString();
            try
            {
                fileToRead = new StreamReader(path);
            }
            catch (DirectoryNotFoundException)  // no directory when opening streamreader for file
            {
                string dirpath = path.Substring(0, path.LastIndexOf('\\')); // get path without filename and extension
                Directory.CreateDirectory(dirpath); // create directories tree
                try
                {
                    fileToRead = new StreamReader(path);
                }
                catch (FileNotFoundException)
                {
                    WriteToFile(dictionary, path);
                }
            }
            catch (FileNotFoundException)   //no such file when opening streamreader
            {
                WriteToFile(dictionary, path); //writing to file default, empty configuration
            }
            if (fileToRead == null) //if still cannot reach file, something really wrong happened
            { }
            else
            {
                string line;
                Dictionary<string, string> temp = new Dictionary<string, string>();
                while ((line = fileToRead.ReadLine()) != null)
                {
                    try
                    {
                        if (line.Contains("|")) //if not, it can't be proper value entry
                        {
                            ParseData(line,
                                out string key,
                                out string value);
                            temp.Add(key, value);
                        }
                    }
                    catch (KeyNotFoundException ex) //key not recognized, skipping
                    {
                        Console.WriteLine(ex.Message);
                        /*MessageBox.Show(ex.Message, Application.Current.Resources
                        ["SettingsErrors_Data_Wrong"].ToString(), MessageBoxButton.OK, MessageBoxImage.Error);*/
                        continue;
                    }
                }
                if (temp.Count == _keys.Count)
                    dictionary = temp;
                else
                {   //config not complete - keys from file not covering keys that are required in code
                    //using values from default dictionary instead
                    foreach (string key in _keys)
                    {
                        if (temp.ContainsKey(key)) dictionary[key] = temp[key];
                    }
                }
            }
            if (fileToRead != null)
            {
                fileToRead.Dispose();
                fileToRead.Close();
            }
            return dictionary;
        }

        public static Dictionary<string, string> GetDataFromFile()
        {
            return GetDataFromFile(PATHS.LASTLOGIN, LASTLOGIN_KEY_COLLECTION.COLLECTION);
        }

        private static void ParseData(string line, out string key, out string value)
        {   //data in file looks like: "KEY|VALUE", we need to separate them
            key = line.Substring(0, line.IndexOf('|'));
            value = line.Substring(line.IndexOf('|') + 1);
            if (!_keys.Contains(key))   //file contains key, that doesn't match any of required keys in collection
            {
                throw new KeyNotFoundException(GetStringResourceSafely("SettingsErrors_Data_Unknown"));
            }
        }

        private static Dictionary<string, string> GenerateDefaultDictionary()
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            foreach (string key in _keys)
            {
                ret.Add(key, "");
            }

            return ret;
        }
    }
}
