﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DC12G_Tester.Files
{
    public class FileOperations
    {
        public static void WriteToFile(Dictionary<string, string> dict, string path)
        {
            string textToWrite = "";
            foreach (KeyValuePair<string, string> pair in dict)
            {
                textToWrite += $"{pair.Key}|{pair.Value}\n";
            }
            List<string> toWrite = new List<string>();
            foreach (string val in textToWrite.Split('\n').ToList())
            {
                if (val.Contains("|"))
                {
                    toWrite.Add(val);
                }
            }
            File.WriteAllLines(path, toWrite);
        }
    }
}
