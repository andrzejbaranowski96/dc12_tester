﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DC12G_Tester.Resources
{
    public static class ResourcesDeployer
    {
        public static string GetStringResourceSafely(string dictionaryKey)
        {
            try
            {
                return Application.Current.Resources[dictionaryKey].ToString();
            } catch (NullReferenceException)
            {
                return "Dictionary not available";
            }
        }
    }
}
