using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestFixture]
    public class SettingsFileTests
    {
        [Test]
        public void LoadingFile_CorrectData_ReturnsTrue()
        {
            string path = Path.GetTempFileName();
            Dictionary<string, string> dict = new Dictionary<string, string>()
            {
                {"KEY1","VALUE1" }, {"KEY","VALUE2" }, {"KEY3","VALUE3" }
            };
            List<string> dataToWrite = new List<string>()
            {
                "KEY1|VALUE1", "KEY2|VALUE2", "KEY3|VALUE3"
            };
            File.WriteAllLines(path, dataToWrite);

            var resultDict = DC12G_Tester.Files.Settings.GetDataFromFile(path);

            Assert.AreEqual(dict, resultDict);
        }
    }
}