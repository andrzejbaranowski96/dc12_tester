﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class EncryptDecryptUnitTests
    {
        private string rawData = "testString[1,2.3]";
        private string encryptedData = "5F+XhlpjZiKHIKXAaFy9hd+HU5u/Wfloz+Cqv+IZRJSkZMOdGdmp0BE2FbfMTDnVAbDc123";
        private string key = "AbDc123";

        [TestMethod]
        public void Encrypt_KeyIsStatic_ReturnsTrue()
        {
            string received = Encrypt(rawData, key);
            Assert.AreEqual(encryptedData, received);
        }

        [TestMethod]
        public void Decrypt_KeyIsStatic_ReturnsTrue()
        {
            string received = Decrypt(encryptedData, key);
            Assert.AreEqual(rawData, received);
        }

        [TestMethod]
        public void Decrypt_KeyFromData_ReturnsTrue()
        {
            string received = DC12G_Tester.Security.Cipher.Decrypt(encryptedData);
            Assert.AreEqual(rawData, received);
        }

        [TestMethod]
        public void EncryptDecrypt_RandomKey_ReturnsTrue()
        {
            string key = GenerateEncryptionKey();

            string encrypted = Encrypt(rawData, key);
            string decrypted = Decrypt(encrypted, key);

            Assert.AreEqual(rawData, decrypted);
        }

        [TestMethod]
        public void AreKeysAppendingCorrectly_ReturnsTrue()
        {
            string key = GenerateEncryptionKey();
            string encrypted = Encrypt(rawData, key);

            Assert.AreEqual(key, encrypted.Substring(encrypted.Length-key.Length));
        }

        [TestMethod]
        public void DecryptEmptyData_ReturnsEmptyString()
        {
            string decrypted = DC12G_Tester.Security.Cipher.Decrypt("");
            Assert.AreEqual(string.Empty, decrypted);
        }

        [TestMethod]
        public void Decrypt_NoKey_ReturnsEmptyString()
        {
            string encrypted = encryptedData.Substring(0, encryptedData.Length - key.Length);
            string decrypted = DC12G_Tester.Security.Cipher.Decrypt(encrypted);
            Assert.AreEqual(string.Empty, decrypted);
        }

        [TestMethod]
        public void Decrypt_TooLongString_ReturnsEmptyString()
        {
            string encrypted = encryptedData + "1";
            string decrypted = DC12G_Tester.Security.Cipher.Decrypt(encrypted);
            Assert.AreEqual(string.Empty, decrypted);
        }

        [TestMethod]
        public void EncryptEmptyString_ReturnsTrue()
        {
            string encrypted = DC12G_Tester.Security.Cipher.Encrypt("");
            string decrypt = DC12G_Tester.Security.Cipher.Decrypt(encrypted);
            Assert.AreEqual(string.Empty, decrypt);
        }

        [TestMethod]
        public void EncryptDecrypt_WrongKey_ReturnsEmptyString()
        {
            string encrypted = Encrypt(rawData, key);
            string decrypted = Decrypt(encrypted, "abcd321");
            Assert.AreEqual(string.Empty, decrypted);
        }

        /* Using reflection to access private methods and invoke them */
        #region Methods
        private string Encrypt(string rawData, string key)
        {
            Type type = typeof(DC12G_Tester.Security.Cipher);
            MethodInfo encrypt_method = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Static)
                .Where(x => x.Name == "Encrypt_UnitTestOnly" && x.IsPrivate).First();
            return (string)encrypt_method.Invoke(null, new object[] { rawData, key });
        }

        private string Decrypt(string encryptedData, string key)
        {
            Type type = typeof(DC12G_Tester.Security.Cipher);
            MethodInfo decrypt_method = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Static)
                .Where(x => x.Name == "Decrypt_UnitTestOnly" && x.IsPrivate).First();
            return (string)decrypt_method.Invoke(null, new object[] { encryptedData, key });
        }

        private string GenerateEncryptionKey()
        {
            Type type = typeof(DC12G_Tester.Security.Cipher);
            MethodInfo generateKey_method = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Static)
                .Where(x => x.Name == "GenerateEncryptionKey_UnitTestOnly" && x.IsPrivate).First();
            return (string)generateKey_method.Invoke(null, null);
        }
        #endregion
    }
}
