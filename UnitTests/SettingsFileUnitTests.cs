﻿using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class SettingsFileUnitTests
    {
        [TestMethod]
        public void GetDataFromFile_DataCorrect()
        {
            //setup
            List<string> dataToSave = new List<string>()
            {
                "KEY1|VALUE1","KEY2|VALUE2","KEY3|VALUE3"
            };
            List<string> keys = new List<string>()
            {
                "KEY1", "KEY2","KEY3"
            };
            Dictionary<string, string> expectedDict = new Dictionary<string, string>()
            {
                {"KEY1","VALUE1" },{"KEY2","VALUE2" },{"KEY3","VALUE3" }
            };
            string path = Path.GetTempFileName();
            File.WriteAllLines(path, dataToSave);

            //run
            var data = DC12G_Tester.Files.Settings.GetDataFromFile(path, keys);

            //compare
            foreach (KeyValuePair<string, string> pair in data)
            {
                if (expectedDict[pair.Key] != data[pair.Key])
                    Assert.Fail($"On <{pair.Key}><{pair.Value}>.");
            }
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void GetDataFromFile_DataEmpty()
        {
            //setup
            List<string> keys = new List<string>()
            {
                "KEY1", "KEY2", "KEY3"
            };
            Dictionary<string, string> expectedDict = new Dictionary<string, string>()
            {
                {"KEY1","" },{"KEY2","" },{"KEY3","" }
            };
            string path = Path.GetTempFileName();

            //run
            var data = DC12G_Tester.Files.Settings.GetDataFromFile(path, keys);

            //compare
            foreach (KeyValuePair<string, string> pair in data)
            {
                if (expectedDict[pair.Key] != data[pair.Key])
                    Assert.Fail($"On <{pair.Key}><{pair.Value}>.");
            }
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void GetDataFromFile_DataNotComplete()
        {
            //setup
            List<string> dataToSave = new List<string>()
            {
                "KEY1|VALUE1","KEY2|VALUE2","KEY3|VALUE3"
            };
            List<string> keys = new List<string>()
            {
                "KEY1", "KEY2", "KEY3", "KEY4", "KEY5"
            };
            Dictionary<string, string> expectedDict = new Dictionary<string, string>()
            {
                {"KEY1","VALUE1" },{"KEY2","VALUE2" },{"KEY3","VALUE3" },{"KEY4","" },{"KEY5","" }
            };
            string path = Path.GetTempFileName();
            File.WriteAllLines(path, dataToSave);

            //run
            var data = DC12G_Tester.Files.Settings.GetDataFromFile(path, keys);

            //compare
            foreach (KeyValuePair<string, string> pair in data)
            {
                if (expectedDict[pair.Key] != data[pair.Key])
                    Assert.Fail($"On <{pair.Key}><{pair.Value}>.");
            }
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void GetDataFromFile_FileNotExists()
        {
            //setup
            List<string> keys = new List<string>()
            {
                "KEY1", "KEY2", "KEY3"
            };
            Dictionary<string, string> expectedDict = new Dictionary<string, string>()
            {
                {"KEY1","" },{"KEY2","" },{"KEY3","" }
            };
            string path = Path.GetTempPath() + "settingsfile.cfg";
            if (File.Exists(path)) File.Delete(path);

            //run
            var data = DC12G_Tester.Files.Settings.GetDataFromFile(path, keys);

            //compare
            foreach (KeyValuePair<string, string> pair in data)
            {
                if (expectedDict[pair.Key] != data[pair.Key])
                    Assert.Fail($"On <{pair.Key}><{pair.Value}>.");
            }
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void GetDataFromFile_IncorrectData()
        {
            //setup
            List<string> dataToSave = new List<string>()
            {
                "KEY1VALUE1","WRONGKEY2|VALUE2","KEY3|VALUE3"
            };
            List<string> keys = new List<string>()
            {
                "KEY1", "KEY2", "KEY3"
            };
            Dictionary<string, string> expectedDict = new Dictionary<string, string>()
            {
                {"KEY1",""}, {"KEY2","" }, {"KEY3","VALUE3"}
            };
            string path = Path.GetTempFileName();
            File.WriteAllLines(path, dataToSave);

            //run
            var data = DC12G_Tester.Files.Settings.GetDataFromFile(path, keys);

            //compare
            foreach (KeyValuePair<string, string> pair in data)
            {
                if (expectedDict[pair.Key] != data[pair.Key])
                    Assert.Fail($"On <{pair.Key}><{pair.Value}>.");
            }
            Assert.IsTrue(true);
        }
    }
}
